from os.path import realpath, abspath, join, dirname
MODULE_PATH = abspath(join(dirname(realpath(__file__)), ".."))
DATA_PATH = join(MODULE_PATH, "data")
TEMP_WAV_FILE = join(DATA_PATH, "tmp.wav")
DEFAULT_TUNING = 440 # [Hz] (A4 frequency)
DEFAULT_LPF = 20 # [Hz]
DEFAULT_PROMINENCE = 0.1
DEFAULT_DISTANCE = 0.1 # [s]
DEFAULT_FREQ_MIN = 80 # [Hz]
DEFAULT_FREQ_MAX = 1500 # [Hz]

