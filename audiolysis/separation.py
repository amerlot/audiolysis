"""
Collection of helper functions
for source separation algorithms
"""

from spleeter.__main__ import separate
import subprocess
from time import time
from sys import exit
from .constants import DATA_PATH


def spleeter_separate(path, mode="spleeter:4stems",
    t_min=0.0, t_max=False):
    """
    Separate sources of a given audio file
    using "spleeter" algorithm implemented by Deezer.

    Arguments:
        path (str): path of the audio file
        mode (str): type of separation among "spleeter:2stems",
            "spleeter:4stems" and "spleeter:5stems"
            Defaults to "spleeter:4stems"
        t_min (float): time where to start separation (in s)
            Defaults to 0.0
        t_max (float): time where to stop separation (in s)
            Defaults to False
    """
    if not t_max:
        duration = 600.0
    else:
        duration = t_max - t_min
    separate(
        deprecated_files=None,
        files=[path],
        adapter="spleeter.audio.ffmpeg.FFMPEGProcessAudioAdapter",
        bitrate="128k",
        codec="wav",
        duration=duration,
        offset=t_min,
        output_path=DATA_PATH,
        stft_backend="auto",
        filename_format="{filename}-{instrument}.{codec}",
        params_filename=mode,
        mwf=False,
        verbose=False)


def demucs_separate(path):
    """
    Separate sources of a given audio file
    using "demucs" algorithm implemented by Facebook.

    Argument:
        path (str): path of the audio file
    """
    cmd = f"python3 -m demucs.separate -d cpu -o {DATA_PATH} -n mdx_extra --shifts=10 {path}"
    cmd = cmd.split(" ")
    subprocess.run(cmd)


def source_separation(path, algo="spleeter",
    mode="spleeter:4stems", t_min=0.0, t_max=False, verbose=True):
    """
    Separate sources of a given audio file
    using "spleeter" or "demucs".

    Arguments:
        path (str): path of the audio file
        algo (str): algorithm to use among "spleeter" or "demucs"
        verbose (bool): whether to print the processing time
            Defaults to True

        Arguments only used by spleeter:
            mode (str): type of separation among "spleeter:2stems",
                "spleeter:4stems" and "spleeter:5stems"
                Defaults to "spleeter:4stems"
            t_min (float): time where to start separation (in s)
                Defaults to 0.0
            t_max (float): time where to stop separation (in s)
                Defaults to False
    """
    start = time()
    if algo == "spleeter":
        spleeter_separate(path, mode, t_min, t_max)
    elif algo == "demucs":
        demucs_separate(path)
    else:
        "Algo should be chosen among spleeter and demucs"
        exit()
    duration = time() - start
    if verbose:
        print(f"{algo}: {duration:.1f}s")

