"""
Definition of class "AudioStream"
to display and visualize an audio waveform
and a frequency analysis
"""

import numpy as np
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import pyaudio
import sys
import wave
import soundfile as sf
from os import remove
import sox
from time import sleep
from .music import analyze
from .constants import TEMP_WAV_FILE, DEFAULT_FREQ_MAX, DEFAULT_FREQ_MIN, DEFAULT_TUNING, DEFAULT_LPF
TRANSPARENT = (0, 0, 0, 0)
BLUE_HL = (0, 100, 200, 60)
MARGIN = 1.2
MAX8BIT = 255
COLOR_ENV = (0, MAX8BIT, MAX8BIT, 200)
FLAG_CONTINUE = pyaudio.paContinue
FLAG_STOP = pyaudio.paComplete
MAX_STREAM_DURATION = 10 # [s]


class AudioStream(object):
    """
    Stream an audio file with a visual animation
    and an optional frequency analysis

    Init arguments:
        data (1D array): audio waveform data
        fs (int): sampling rate of the audio file (in Hz)
        t_min (float): Time (in s) where to begin the stream
            Defaults to 0
        t_max (float): Time (in s) where to end the stream
            Defaults to False
        n_loop (int): Number of loops
            Defaults to 1
        speed_ratio (float): speed ratio at which to display
            the stream
            Defaults to 1
        analysis (bool): whether to display the frequency analysis
            Defaults to False
        tuning (float): frequency to use as the A4 reference (in Hz).
            If set to -1, the best tuning will be detected automatically.
            Defaults to 440 Hz
        freq_min (float): minimum frequency for pitch detection (in Hz)
            Defaults to 80 Hz
        freq_max (float): maximum frequency for pitch detection (in Hz)
            Defaults to 80 Hz
        lpf (float): low pass filter cut-off frequency (in Hz)
            Defaults to 20 Hz
        close_at_end (bool): whether to close the graphic window
            at the end of the stream
            Defaults to False
        anim (bool): whether to display the animated lines
            Defaults to True
    """
    def __init__(self, data, fs, t_min=0, t_max=False, n_loop=1,
        speed_ratio=1, analysis=False, tuning=DEFAULT_TUNING,
        freq_min=DEFAULT_FREQ_MIN, freq_max=DEFAULT_FREQ_MAX,
        lpf=DEFAULT_LPF, close_at_end=False, anim=True):

        # get arguments
        self.anim = anim
        self.close_at_end = close_at_end
        self.analysis = analysis
        self.n_loop = n_loop
        self.speed_ratio = speed_ratio
        self.data = data

        # slice data according to t_min and t_max
        if not t_max:
            t_max = t_min + MAX_STREAM_DURATION
        self.data = self.data[:int(round(t_max * fs)) + 1]
        self.data = self.data[int(round(t_min * fs)):]

        # write as .wav file to be used by pyaudio
        sf.write(TEMP_WAV_FILE, self.data, fs)

        # stretch .wav file to change speed
        if speed_ratio != 1:
            y, sr = sf.read(TEMP_WAV_FILE)
            tfm = sox.Transformer()
            tfm.tempo(speed_ratio, audio_type="m")
            y_stretch = tfm.build_array(input_array=y, sample_rate_in=sr)
            sf.write(TEMP_WAV_FILE, y_stretch, sr)

        # open streched .wav file using wave module
        self.wf = wave.open(TEMP_WAV_FILE, "rb")
        remove(TEMP_WAV_FILE)

        # define chunk size (for stream and visual update)
        self.chunk = 1024 * 2

        # prepare stream data
        self.n_updates = int(np.floor(len(self.data) / float(self.chunk * self.speed_ratio)))
        self.stream_data = [self.wf.readframes(self.chunk) for _ in range(self.n_updates)]

        # define a window to visualize the audio waveform
        pg.setConfigOptions(antialias=True)
        self.win = pg.GraphicsWindow(title="Audio Viewer")
        if self.analysis:
            self.win.setGeometry(40, 100, 1800, 800)
        else:
            self.win.setGeometry(40, 100, 1800, 600)

        # define time axis
        self.t = (np.arange(len(self.data)) / float(fs)) + float(t_min)
        self.dt = (self.t[self.chunk] - self.t[0]) * self.speed_ratio
        self.idx_update, self.line_x_pos, self.idx_loop = 0, self.t[0], 0
        self.flag = FLAG_CONTINUE

        # define first plot item
        self.audio = self.win.addPlot(row=1, col=1)
        self.audio.setXRange(self.t[0], self.t[-1], padding=0)
        self.y_max = np.max(np.abs(self.data)) * MARGIN
        self.audio.setYRange(- self.y_max, self.y_max, padding=0)
        self.audio.setLabel("left", "Waveform")
        self.audio.getAxis("left").setTicks([[]])
        if not self.analysis:
            self.audio.setLabel("bottom", "Time [s]")
        else:
            self.audio.getAxis("bottom").setTicks([[]])

        # define a first curve item for the audio waveform
        self.waveform = self.audio.plot(pen="c")
        self.waveform.setData(self.t, self.data)

        # define a second curve item for the line showing the chunk being displayed
        self.line = self.audio.addLine(x=self.line_x_pos, pen="y")

        # add a second plot item for analysis
        if self.analysis:
            env, highlight, notes = analyze(self.data, fs, lpf=lpf,
                tuning=tuning, freq_min=freq_min, freq_max=freq_max)
            self.anlz = self.win.addPlot(row=2, col=1)
            self.anlz.setXRange(self.t[0], self.t[-1], padding=0)
            self.anlz.setYRange(0, MARGIN, padding=0)
            self.anlz.setLabel("left", "Analysis")
            self.anlz.setLabel("bottom", "Time [s]")
            self.anlz.getAxis("left").setTicks([[]])
            # plot audio envelope
            self.env = self.anlz.plot(pen=COLOR_ENV)
            self.env.setData(self.t, env)
            all_notes = [_[0] for n in notes for _ in n]
            self.note_min, self.note_max = np.min(all_notes), np.max(all_notes)
            # for each note detected
            for _, h in enumerate(highlight):
                # highlight the part
                l, r = self.t[h[0]], self.t[h[1]]
                hl = pg.LinearRegionItem(values=(l, r), pen=TRANSPARENT, brush=BLUE_HL, hoverPen=TRANSPARENT)
                self.anlz.addItem(hl)
                t_note = 0.5 * (l + r)
                # print the name of the notes detected there
                for n, a, name in notes[_]:
                    y_note = self.note_to_y(n)
                    txt = pg.TextItem(name, color=(MAX8BIT, MAX8BIT, MAX8BIT, int(round(a * MAX8BIT))), anchor=(0.5, 0.5))
                    self.anlz.addItem(txt)
                    txt.setPos(t_note, y_note)
                    font = QtGui.QFont()
                    font.setPixelSize(16)
                    txt.setFont(font)
            self.line2 = self.anlz.addLine(x=self.line_x_pos, pen="y")

        # put wav file into a pyaudio streamer
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(
            format=self.p.get_format_from_width(self.wf.getsampwidth()),
            channels=self.wf.getnchannels(),
            rate=fs,
            frames_per_buffer=self.chunk,
            output=True,
            stream_callback=self.callback)

    def note_to_y(self, note):
        # rescale note pitch
        notes_0 = self.note_min - 3
        notes_amp = self.note_max - self.note_min + 6
        ratio = (note - notes_0) / notes_amp
        return ratio * MARGIN

    def start(self):
        # to start the animation
        if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
            self.app = QtGui.QApplication.instance()
            self.app.aboutToQuit.connect(self.stop_audio)
            self.app.exec_()
        self.stream.start_stream()

    def stop_audio(self):
        # to end audio stream
        self.flag = FLAG_STOP

    def callback(self, *args, **kwargs):
        # add delay to avoid lag at first call
        if self.idx_update == 0 and self.idx_loop == 0:
            sleep(0.5)
        # get data to stream
        to_stream = self.stream_data[self.idx_update]
        # update idx_update and line_x_pos
        self.idx_update += 1
        self.line_x_pos += self.dt
        # at the end of a loop:
        if self.idx_update >= self.n_updates:
            self.idx_loop += 1
            # check if we need to loop again...
            if self.idx_loop < self.n_loop:
                self.idx_update = 0
                self.line_x_pos = self.t[0]
            # ...or to end the stream
            else:
                self.flag = FLAG_STOP
        # return data to stream
        return (to_stream, self.flag)

    def update(self):
        # update line curve item
        if self.anim:
            self.line.setValue(self.line_x_pos)
            if self.analysis:
                self.line2.setValue(self.line_x_pos)
        # exit
        if self.flag == FLAG_STOP:
            if self.close_at_end:
                self.win.close()

    def run(self):
        # to run the animation
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(15)
        self.start()

