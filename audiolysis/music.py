"""
Collection of helper functions
for music analysis
"""

import numpy as np
import scipy.signal as ss
from scipy.fft import rfft, rfftfreq
import matplotlib.pyplot as plt
from .constants import DEFAULT_LPF, DEFAULT_PROMINENCE, DEFAULT_DISTANCE, DEFAULT_FREQ_MIN, DEFAULT_FREQ_MAX, DEFAULT_TUNING
NOTES = ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"]
OCTAVE = 12
A4_note_number = 49
GAP_BETWEEN_C_AND_A = 8
MAX_COMPONENTS = 4
DETECT_TUNING = -1
gap_between_two_detected_components = 20 # [Hz]


def get_envelope(data, fs, lpf=DEFAULT_LPF):
    """
    Compute the envelope of a given audio waveform
    using an Hilbert transform and a low pass filter

    Arguments:
        data (1D array): audio waveform data
        fs (int): sampling rate of the audio file (in Hz)
        lpf (float): low pass filter cut-off frequency (in Hz)
            Defaults to 20 Hz

    Returns:
        env (1D array): computed audio envelope
    """
    env = np.abs(ss.hilbert(data))
    b, a = ss.butter(2, lpf, btype="low", fs=fs)
    env = ss.filtfilt(b, a, env)
    return env


def separate_bases(peaks, bases, env, fs):
    """
    Separate the potential overlapping bases detected
    on the audio envelope. When overlapping bases are found:
        - if there are > 2 overlapping bases, the largest one
        is deleted
        - otherwise, since there are 2 overlapping bases, the local
        minimum between the 2 peaks is chosen to be the right side of
        the base on the left and the left side of the base on the right

    Arguments:
        peaks (1D array): locations of the peaks detected on
            the audio envelope
        bases (2D array): locations of the left and right
            sides of these peaks
        env (1D array): audio waveform data
        fs (int): sampling rate of the audio file (in Hz)

    Returns:
        peaks (1D array): locations of the separated peaks
        bases (2D array): locations of the separated bases
    """
    while True:
        for (l, r) in bases:
            for s in [l, r]:
                x = [[_, b[1] - b[0]] for _, b in enumerate(bases) if b[0] <= s and b[1] >= s]
                if len(x) > 1:
                    idx = x[np.argmax([e[1] for e in x])][0]
                    if len(x) == 2:
                        idx_l, idx_r = x[0][0], x[1][0]
                        p_l, p_r = peaks[idx_l], peaks[idx_r]
                        mid = np.argmin(env[p_l:p_r]) + p_l
                        margin = int(round(0.01 * fs))
                        bases[idx_l, 1] = mid - margin
                        bases[idx_r, 0] = mid + margin
                    else:
                        bases = np.delete(bases, idx, axis=0)
                        peaks = np.delete(peaks, idx, axis=0)
        break
    return peaks, bases


def analyze(data, fs, lpf=DEFAULT_LPF, distance=DEFAULT_DISTANCE,
    prominence=DEFAULT_PROMINENCE, tuning=DEFAULT_TUNING,
    freq_min=DEFAULT_FREQ_MIN, freq_max=DEFAULT_FREQ_MAX):
    """
    Frequency analysis of an audio waveform given a set
    of parameters. The main steps of this analysis are:
        - computation of the audio envelope
        - normalization of this envelope
        - detection of peaks and corresponding bases on this envelope
        - separation of potential overlapping bases
        - for each base (which should correspond to a note), detection
            of the main frequency components

    Arguments:
        data (1D array): audio waveform data
        fs (int): sampling rate of the audio file (in Hz)
        lpf (float): low pass filter cut-off frequency (in Hz)
            Defaults to 20 Hz
        distance (float): distance (in s) to be used for peak detection
            on the normalized envelope
            Defaults to 0.1 s
        prominence (float): prominence (ratio) to be used for peak detection
            on the normalized envelope
            Defaults to 0.1
        tuning (float): frequency to use as the A4 reference (in Hz).
            If set to -1, the best tuning will be detected automatically.
            Defaults to 440 Hz
        freq_min (float): minimum frequency for pitch detection (in Hz)
            Defaults to 80 Hz
        freq_max (float): maximum frequency for pitch detection (in Hz)
            Defaults to 80 Hz

    Returns:
        env (1D array): computed audio envelope
        bases (2D array): left and right sides of peaks detected on envelope
        notes (list): list of list of note numbers/importances/names detected within each base
    """
    env = get_envelope(data, fs, lpf)
    env = (env - np.min(env)) / (np.max(env) - np.min(env))
    peaks, properties = ss.find_peaks(env, height=0.3,
        distance=int(round(distance * fs)), prominence=prominence, width=1, rel_height=0.7)
    bases = np.vstack((properties["left_ips"], properties["right_ips"])).T
    bases = np.round(bases).astype(np.int32)
    peaks, bases = separate_bases(peaks, bases, env, fs)
    if tuning == DETECT_TUNING:
        err = []
        for (l, r) in bases:
            err.extend(get_freq_components(data[l:r], fs,
                tuning=DEFAULT_TUNING, freq_min=freq_min, freq_max=freq_max,
                detect_tuning=True))
        tuning = DEFAULT_TUNING * np.mean(err)
        print(f"best tuning detected = {tuning:.1f} Hz")
    notes = []
    for (l, r) in bases:
        notes_info, _ = get_freq_components(data[l:r], fs,
            tuning=tuning, freq_min=freq_min, freq_max=freq_max)
        notes.append(notes_info)
    return env, bases, notes


def get_note_number_from_freq(freq, tuning=DEFAULT_TUNING,
    detect_tuning=False):
    """
    Compute the note number (for instance, the note number of A4
    is 49) corresponding to a given frequency, in a given tuning.
    We assume the use of a 12-notes tempered scale, in which
    the frequency ratio between two successive notes is equal
    to 2 ** (1 / 12).

    Arguments:
        freq (float): frequency (in Hz) to be converted in note number
        tuning (float): frequency (in Hz) of A4 note (i.e. the tuning)
            Defaults to 440 Hz
        detect_tuning (bool): Whether the function is used to detect the
            best tuning to use.
            Defaults to False

    Returns:
        note_number (int): note number corresponding to the input frequency
    """
    n = len(NOTES) * (np.log(freq) - np.log(tuning)) / np.log(2)
    if detect_tuning:
        return (np.round(n) / n)
    note_number = int(A4_note_number + np.round(n))
    return note_number


def get_note_name(note_number):
    """
    Get the note name of a given note number. Each note has an integer
    representation (i.e. 49 or 62) and a string representation
    (i.e. "A4" or "Bb5") in the form f"{note_name}{octave_number}"

    Argument:
        note_number (int): integer representation of a note

    Returns:
        note_name (str): string representing the note
    """
    note_number += GAP_BETWEEN_C_AND_A
    octave = note_number // OCTAVE
    return f"{NOTES[note_number % OCTAVE]}{octave}"


def get_freq_components(data, fs, tuning=DEFAULT_TUNING,
    freq_min=DEFAULT_FREQ_MIN, freq_max=DEFAULT_FREQ_MAX,
    detect_tuning=False):
    """
    Get main frequency components of a note given its audio
    waveform. Here are the main steps of this operation:
        - computation of the FFT of the audio data
        - restriction of this FFT to (freq_min, freq_max)
        - normalization of the FFT
        - detection of a maximum of 4 peaks on the FFT ("components")
        - conversion of the peak values (in Hz) to note_numbers,
            importances (y-value on the normalized FFT) and names

    Arguments:
        data (1D array): audio waveform data
        fs (int): sampling rate of the audio file (in Hz)
        tuning (float): frequency to use as the A4 reference (in Hz)
            Defaults to 440 Hz
        freq_min (float): minimum frequency for pitch detection (in Hz)
            Defaults to 80 Hz
        freq_max (float): maximum frequency for pitch detection (in Hz)
            Defaults to 80 Hz
        detect_tuning (bool): Whether the function is used to detect the
            best tuning to use.
            Defaults to False

    Returns:
        notes_info (list): for each detected frequency component
            are given the note number, the importance and the name
        to_plot (dict): in order to use debug function "plot_fft"
    """
    n = len(data)
    freq = rfftfreq(n, 1 / fs)
    fft = np.abs(rfft(data - np.mean(data)))
    idx_min = np.where(freq >= freq_min)[0][0]
    idx_max = np.where(freq <= freq_max)[0][-1]
    freq, fft = freq[idx_min:idx_max], fft[idx_min:idx_max]
    fft = fft / np.max(fft)
    distance = int(np.ceil(gap_between_two_detected_components / ((freq_max - freq_min) / len(freq))))
    components = ss.find_peaks(fft, height=0.2, distance=distance)
    components = components[0][np.argsort(components[1]["peak_heights"])[::-1]]
    if len(components) > MAX_COMPONENTS:
        components = components[:MAX_COMPONENTS]
    note_numbers = [get_note_number_from_freq(freq[p], tuning=tuning, detect_tuning=detect_tuning) for p in components]
    if detect_tuning:
        return note_numbers
    notes_info = [[note_numbers[i], fft[p], get_note_name(note_numbers[i])] for i, p in enumerate(components)]
    to_plot = {"freq": freq, "fft": fft, "components": components, "xlim": (freq_min, freq_max)}
    return notes_info, to_plot


def plot_fft(to_plot):
    """
    Debug function to plot FFT of a note.

    Argument:
        to_plot (dict): output by function "get_freq_components"
    """
    freq, fft, components = to_plot["freq"], to_plot["fft"], to_plot["components"]
    fig = plt.figure(figsize=(12, 8))
    plt.plot(freq, fft)
    for p in components:
        plt.scatter(freq[p], fft[p], color="C1", marker="o", s=40)
        plt.text(freq[p], fft[p] + 20, get_note_name(get_note_number_from_freq(freq[p])), ha="center", va="center", fontsize=14)
    plt.xlim(to_plot["xlim"])
    plt.ylim(-50, np.max(fft) + 50)
    plt.ylabel("FFT")
    plt.xlabel("Frequency [Hz]")
    plt.show()

