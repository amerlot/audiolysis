"""
Collection of helper functions
for reading and playing audio files
"""

import warnings
warnings.filterwarnings("ignore")
import IPython.display as ipd
import librosa


def get_audio(path):
    """
    Get the audio data of an audio file
    given its path

    Argument:
        path (str): path of the audio file

    Returns:
        data (1D array): audio waveform data
        fs (int): sampling rate of the audio file (in Hz)
    """
    data, fs = librosa.load(path, sr=None)
    return data, fs


def play_audio(data, fs):
    """
    Display audio data (only in Jupyter Lab)

    Arguments:
        data (1D array): audio waveform data
        fs (int): sampling rate of the audio file (in Hz)
    """
    ipd.display(ipd.Audio(data=data, rate=fs))

