"""
Collection of helper functions
for searching and downloading on YouTube
"""

from youtubesearchpython import VideosSearch
import youtube_dl
from IPython.display import Image, display
from .constants import DATA_PATH, join
DEFAULT_LIMIT = 3
SEARCH_WIDTH = 60


def search_on_youtube(search_str, limit=DEFAULT_LIMIT):
    """
    Print the results of a YouTube search given a search string
    and a limit on the number of results.

    Arguments:
        search_str (str): string representing the search to do
            on YouTube
        limit (int): limit on the number of results
            Defaults to 3

    Returns:
        search (list): list of dictionaries containing info on each
            video (name, duration, views, link)
    """
    search = VideosSearch(search_str, limit=limit).result()["result"]
    print("\nYouTube Search Results:\n")
    for _, r in enumerate(search):
        print("=" * SEARCH_WIDTH)
        print(f" Video n°{_} ".center(SEARCH_WIDTH, "="))
        print("=" * SEARCH_WIDTH + "\n")
        url = r["thumbnails"][0]["url"].split(".jpg?")[0] + ".jpg"
        display(Image(url=url, width=300))
        title, views, duration = r["title"], r["viewCount"]["short"], r["duration"]
        print(f"\n{title[:SEARCH_WIDTH]}")
        print(f"{duration} // {views}\n\n".rjust(SEARCH_WIDTH))
    return search


def get_link_from_youtube_search(search):
    """
    Get link of the video the user wants to download
    from a search.

    Argument:
        search (list): list of dictionaries containing info on each
            video (name, duration, views, link)

    Returns:
        link (str): link to the video to download
    """
    video_id = input("\n\nWhich video would you like to download? ")
    link = search[int(video_id)]["link"]
    return link


def download_from_youtube(link, filename=""):
    """
    Download a video from YouTube given its link.

    Arguments:
        link (str): link to the video to download
        filename (str): name of the file where to download the video
    """
    if filename == "":
        filename = input("\nOutput filename? ")
    filename = join(DATA_PATH, filename)
    if not filename.endswith(".mp3"):
        filename = f"{filename}.mp3"
    ydl_opts = {
        "format": "bestaudio/best",
        "postprocessors": [{
            "key": "FFmpegExtractAudio",
            "preferredcodec": "mp3",
            "preferredquality": "128"}],
        "outtmpl": filename}
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([link])


def search_and_download_from_youtube(search_str,
    limit=DEFAULT_LIMIT, filename=""):
    """
    Perform a search on YouTube, print the results, and
    download one video chosen by the user.

    Arguments:
        search_str (str): string representing the search to do
            on YouTube
        limit (int): limit on the number of results
            Defaults to 3
        filename (str): name of the file where to download the video
    """
    search = search_on_youtube(search_str, limit=limit)
    link = get_link_from_youtube_search(search)
    download_from_youtube(link, filename=filename)

