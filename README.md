# audiolysis

Setup
=====

Install some packages:

`sudo apt install ffmpeg portaudio19-dev libasound2-dev python3-pyaudio libsndfile1-dev soundstretch sox qt5-default`

Then, in a virtualenv, install some pip modules:

`pip install -r requirements.txt`

Test
====

You can test everything using the notebook `demo.ipynb` in Jupyter Lab

